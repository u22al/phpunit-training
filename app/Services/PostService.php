<?php
/**
 * @author: Mahabubul Hasan <codehasan@gmail.com>
 */

namespace App\Services;


use App\Post;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class PostService
{
    public function add($a, $b){
        return $a+$b;
    }

    public function apiCall($price){
        $service = new RemoteService();
        return [
            'discounted_price' => $price - ($service->discount() * $price/100)
        ];
    }

    public function validator($data){
        return Validator::make($data, [
            'title' => 'required | max:150',
            'description' => 'required'
        ]);
    }

    public function create(Request $req){
        return Post::create([
            'title' => $req->title,
            'description' => $req->description
        ]);
    }

    public function getRow($id){
        return Post::find($id);
    }

    public function getAll(){
        return Post::all();
    }

    public function update($id, Request $req){
        return Post::find($id)->update([
            'title' => $req->title,
            'description' => $req->description
        ]);
    }

    public function delete($id){
        return Post::destroy($id);
    }


}