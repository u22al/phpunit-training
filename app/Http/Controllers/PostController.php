<?php
/**
 * @author: Mahabubul Hasan <codehasan@gmail.com>
 */

namespace App\Http\Controllers;


use App\Services\PostService;
use Illuminate\Http\Request;

class PostController extends Controller
{
    private $_service;

    public function __construct(PostService $service)
    {
        $this->_service = $service;
    }

    public function index(){
        return view('post.index', ['rows'=>$this->_service->getAll()]);
    }

    public function create(){
        return view('post.create');
    }

    public function store(Request $request){
        $this->_service->validator($request->all())->validate();
        $resp = $this->_service->create($request);
        if($resp){
            return redirect('post')->with('msg', 'Post created successfully!');
        }
    }

    public function edit($id){
        return view('post.edit', [
            'row' => $this->_service->getRow($id)
        ]);
    }

    public function update($id, Request $request){

    }

    public function delete($id){

    }

}