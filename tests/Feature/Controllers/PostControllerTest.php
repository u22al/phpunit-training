<?php
/**
 * @author: Mahabubul Hasan <codehasan@gmail.com>
 */

namespace Tests\Feature\Controllers;


use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Support\Facades\Artisan;
use Tests\TestCase;

class PostControllerTest extends TestCase
{
    //use RefreshDatabase;

    protected function setUp()
    {
        parent::setUp();
        Artisan::call('migrate');
    }

    protected function tearDown()
    {
        parent::tearDown();
    }

    public function testIndex(){
        $resp = $this->get('post');
        $resp->assertStatus(200);
        $resp->assertSeeText('Post List');
        $resp->assertSeeText('Operations');
    }

    public function testCreate(){
        $resp = $this->get('post/create');
        $resp->assertStatus(200);
        $resp->assertSeeText('Create');
    }

    public function testStore(){
        $this->_testValidation();
        $this->_testInsert();
    }

    private function _testInsert(){
        $resp = $this->post('post', [
            'title'=> 'Sample title',
            'description' => 'Sample description'
        ]);

        $resp->assertStatus(302);
        $this->assertDatabaseHas('posts', [
            'title' => 'Sample title',
            'description' => 'Sample description'
        ]);
    }

    private function _testValidation(){
        $resp = $this->post('post', [
            'description' => 'Sample description'
        ]);

        $resp->assertStatus(302);
        $resp->assertSessionHas('errors');
    }

}