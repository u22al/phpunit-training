<?php
/**
 * @author: Mahabubul Hasan <codehasan@gmail.com>
 */

namespace Tests\Unit\Services;


use App\Services\PostService;
use App\Services\RemoteService;
use Tests\TestCase;
use Mockery as m;

class PostServiceTest extends TestCase
{

    /**
     * @var PostService
     */
    private $_service;

    public function setUp()
    {
        parent::setUp();
        $this->_service = new PostService();
    }

    public function testAdd()
    {
        $a = 2;
        $b = 3;
        $c = 5;

        $this->assertEquals($c, $this->_service->add($a, $b));
    }

    public function testApiCall()
    {
        $remoteService = m::mock('overload:'.RemoteService::class);
        $remoteService->shouldReceive('discount')->andReturn(10);

        $output = $this->_service->apiCall(100);
        $this->assertArrayHasKey('discounted_price', $output);
        $this->assertEquals(90, $output['discounted_price']);
    }

    /**
     * @test
     */
    public function insert()
    {
        $this->assertTrue(true);
    }


}