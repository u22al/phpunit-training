@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header">Post List</div>
                    <a href="{{ url('post/create') }}"></a>
                    <div class="card-body">
                        <div class="table">
                            <table class="table">
                                <thead>
                                <tr>
                                    <th>ID</th>
                                    <th>Name</th>
                                    <th>Description</th>
                                    <th>Operations</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($rows as $r)
                                <tr>
                                    <td>{{$r->post_id}}</td>
                                    <td>{{$r->title}}</td>
                                    <td>{{$r->description}}</td>
                                    <td>
                                        <a href="{{url('post', ['id'=>$r->post_id])}}">Edit</a>
                                        <a onclick="return confirm('Are you sure you want to delete?')" href="{{url('post/delete', ['id'=>$r->post_id])}}">Delete</a>
                                    </td>
                                </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
